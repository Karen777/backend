# Image docker base inicial
FROM node:latest

#Crear directorio de trabajo del contenedor Docker
WORKDIR /docker-dir-apitechu

#Copiar archivos del proyecto en el directorio trabajo Docker
ADD . /docker-dir-apitechu

#Instalar dependencias del proyecto
#RUN npm install --only=production

#Puerto donde exponemos contenedor (mismo que usamos en nuestra API)
EXPOSE 3000

#Comando para lanzar la app
CMD ["npm","run","prod"]
