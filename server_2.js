require ('dotenv').config();
const express = require ('express');//import
const body_parser = require('body-parser');// neceario para el POST
const requestJson = require('request-json');
const cors = require('cors');
const app = express();
//const port = 3000;
const port = process.env.PORT || 3000;
const userFile = require('./user.json');
const URL_BASE = '/apitechu/v3/';
const URL_MLAB_BASE = 'https://api.mlab.com/api/1/databases/techu17db/collections/';
const apiKey_mlab = 'apiKey='+ process.env.API_KEY_MLAB;
var newID =0;

 // asincrono, se agrego function solo para el POST
app.listen(port,function(){
  console.log('Node JS escuchando en el puerto' + port);
});

app.use(cors());
app.options('*', cors());
//  function (){
//var client = requestJson.createCliente(URL_mlab+apikey_mlab);
//});
//necesario para el POST
app.use(body_parser.json());

//Collection con mLab
app.get(URL_BASE + 'users',
function (req,res){
  console.log('hola Perú');
  console.log('hola Lima');
  const http_client = requestJson.createClient(URL_MLAB_BASE);
  console.log('cliente http a mlab ok');
  let field_param = 'f={"_id":0}&';
  http_client.get('user?' + field_param + apiKey_mlab,
      function(error, res_mlab, body){
        console.log('error:'+ error);
        console.log('Mlab: ' + res_mlab);
        console.log('Body: ' + body);
        //response.send(body);
        var response = {};
        if (error){
          response = {"msg": "Error al recupero de datos mLab"};
          res.status(500);
        } else {
          if(body.length > 0){
            response=body;
          } else {
            response = {"msg":"Usuario no encontrado"};
            res.status(404);
          }
        }
        res.send(response);
      });

  //response.status(200).send({'msg':'cliente http a mlab ok'});//la respuesta
});

//instancia
//peticion get a un unico usuario
app.get(URL_BASE + 'users/:id',
function (req,res){
  console.log('GET/apitechu/v3/users/:id');
  console.log(req.params.id);
  let id = req.params.id;
  let queryString = 'q={"id":' + id + '}&';
  let queryStrField = 'f={"_id":0}&';
  let http_client = requestJson.createClient(URL_MLAB_BASE);
  http_client.get('user?'+ queryString + queryStrField + apiKey_mlab,
function(error, res_mlab, body){
  let response ={};
  if (error){
    response ={"msg": "Error en la peticion a mlab"};
    res.status(500);
  } else {
    if (body.length > 0){
      response = body;
    }else {
        response = {"msg":"Usuario no encontrado"};
        res.status(404);
      }
    }
    res.send(response);

});

});

//get con :id y con :account
app.get(URL_BASE + 'users/:id/accounts',
function(req,res){
  console.log('GET/apitechu/v3/users/:id/accounts');
  console.log(req.params.id);
  //console.log(req.params.accounts);
  let id = req.params.id;
  //let accounts = req.params.accounst;
  let queryString = `q={"id":  ${id} + }&`;
  let queryStrField = 'f={"_id":0,"account":1}&';
  //let queryStrCuenta = 'f={"account"<>0}&';
  let http_client = requestJson.createClient(URL_MLAB_BASE);
  http_client.get('user?'+ queryString + queryStrField + apiKey_mlab,
function(error, res_mlab, body){
  let response ={};
  if (error){
    response ={"msg": "Error en la peticion a mlab"};
    res.status(500);
  } else {
    if (body.length > 0){
      response = body;
    }else {
        response = {"msg":"Usuario no encontrado"};
        res.status(404);
      }
    }
    res.send(response);

});
});

//get detalle de una cuenta (movimientos)
app.get(URL_BASE + 'users/:id/accounts/:idaccount',
function(req,res){
  console.log('GET/apitechu/v3/users/:id/accounts/:idaccount');
  console.log(req.params.id);
  console.log(req.params.idaccount);
  //console.log(req.params.accounts);
  let id = req.params.id;
  let idaccount =req.params.idaccount;
  //let accounts = req.params.accounst;
  let queryString = `q={"id":  ${id} , "idAccount":  ${idaccount} + }&`;
  let queryStrField = 'f={"_id":0,"transaction":1}&';
  //let queryStrCuenta = 'f={"account"<>0}&';
  let http_client = requestJson.createClient(URL_MLAB_BASE);
  http_client.get('account?'+ queryString + queryStrField + apiKey_mlab,
function(error, res_mlab, body){
  let response ={};
  if (error){
    response ={"msg": "Error en la peticion a mlab"};
    res.status(500);
  } else {
    if (body.length > 0){
      response = body;
    }else {
        response = {"msg":"Cuenta no encontrada"};
        res.status(404);
      }
    }
    res.send(response);

});
});
//Peticion PUT para actualizar monto con mlab
app.put(URL_BASE + 'users/:id/accounts/:idaccount',
    function (req, res) {
        console.log('PUT/apitechu/v3/users/:id/accounts/:idaccount');
        console.log(req.params.id);
        console.log(req.params.idaccount);
        let id = req.params.id;
        let idaccount = req.params.idaccount;
        var resta;
        var queryStringID = `q={"id":  ${id} , "idAccount":  ${idaccount} + }&`;
        //var queryStringID = 'q={"id":' + id + '}&';
        var clienteMlab = requestJson.createClient(URL_MLAB_BASE);
        clienteMlab.get('account?' + queryStringID + apiKey_mlab,
            function (error, respuestaMlab, body) {
                console.log("antes", body);
                //  var monto =JSON.stringify(body.disponible.value);
                var monto = body[0].disponible; //trae el monto almacenado
                //  var resta =JSON.stringify(req.body.value);
                resta = req.body.disponible; //si trae el retiro
                var result = monto - resta;
                var newMonto = {
                    "disponible": result
                };
                console.log("despues", monto, resta, result);
                var cambio = '{"$set":' + JSON.stringify(newMonto) + '}';
                console.log("cambio ", cambio);
                clienteMlab.put(URL_MLAB_BASE + 'account?' + queryStringID + apiKey_mlab, JSON.parse(cambio),
                    function (error, respuestaMlab, body) {
                        console.log("body" + body);
                        let queryStrField = 'f={"_id":0,"transaction":1}&';
                        clienteMlab.get('account?' + queryStringID + queryStrField + apiKey_mlab,
                            function (error, respuestaMlab, body) {
                                console.log("body", body);
                                let arrayMovimiento = body[0].transaction;
                                newID = body[0].transaction.length + 1;
                                numMovi = body[0].transaction[newID - 2].number + 1;
                                var inicio = Date.now();
                                var fechaString = "\/Date(" + inicio + ")\/";
                                fechaString = fechaString.substr(6)
                                var fechaActual = new Date(parseInt(fechaString));
                                var mes = fechaActual.getMonth() + 1;
                                var dia = fechaActual.getDate();
                                var anio = fechaActual.getFullYear();
                                var fecha = dia + "/" + mes + "/" + anio;
                                console.log(fecha, inicio);
                                var newMovi = {
                                    "id": newID,
                                    "number": numMovi,
                                    "data": fecha,
                                    "amount": resta,
                                    "descripcion": "Retiro de Cajero"
                                };
                                arrayMovimiento.push(newMovi);
                                //console.log("newMovi",newMovi);
                                //console.log("arrayMovimiento: ",arrayMovimiento);
                                var cambioT = {
                                    transaction: arrayMovimiento
                                }
                                var cambioMovi = '{"$set":' + JSON.stringify(cambioT) + '}';
                                console.log("cambioMovi", cambioMovi);
                                clienteMlab.put(URL_MLAB_BASE + 'account?' + queryStringID + queryStrField + apiKey_mlab, JSON.parse(cambioMovi),
                                    function (error, respuestaMlab, body) {
                                        console.log("se muestra lo que ingreso", body);
                                        //kk
                                        let queryStringU = `q={"id":  ${id} + }&`;
                                        let queryStrFieldU = 'f={"_id":0,"account":1}&';
                                        //let http_client = requestJson.createClient(URL_MLAB_BASE);
                                        clienteMlab.get('user?'+ queryStringU + queryStrFieldU + apiKey_mlab,
                                      function(error, res_mlab, body){
                                        let response ={};
                                        if (error){
                                          response ={"msg": "Error en la peticion a mlab"};
                                          res.status(500);
                                        } else {
                                          if (body.length > 0){
                                            response = body;
                                            let Accounts=body[0].account;
                                            console.log("Accounts",Accounts);
                                            console.log("Accounts[idaccount-1]",Accounts[idaccount-1]);
                                            Accounts[idaccount-1].saldo=result;
                                            var cambiou = {
                                                account: Accounts
                                            }
                                            var cambioUser = '{"$set":' + JSON.stringify(cambiou) + '}';
                                            console.log("cambioUser",cambioUser);
                                            clienteMlab.put('user?'+ queryStringU + queryStrFieldU + apiKey_mlab, JSON.parse(cambioUser),
                                          function(error, res_mlab, body){
                                            //res.send(body);
                                            console.log("cambio exitoso");
                                          });

                                          }else {
                                              response = {"msg":"Usuario no encontrado"};
                                              res.status(404);
                                            }
                                          }
                                        //  res.send(response);
                                        //  res.send(body);
                                      });
                                        //kk
                                    });
                            });
                        res.send("Terminado");
                    });
            });
    });


//get con QUERY
app.get(URL_BASE + 'usersq',
function(req,res){
  console.log(req.query.id);
  console.log(req.query.email);
  res.send({"msg":"GET con query"});
});

//Peticion POST a usuario mlab
app.post(URL_BASE + 'users',
function(req,res){
  var clienteMlab = requestJson.createClient(URL_MLAB_BASE);
  console.log(req.body);
  clienteMlab.get('user?'+ apiKey_mlab,
function(error, respuestaMlab, body){
  newID = body.length + 1;
  console.log("newID: "+ newID);
  var newUser = {
    "id" : newID,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": req.body.password
  };
  clienteMlab.post("user?"+apiKey_mlab, newUser,
function(error,respuestaMlab, body){
  console.log(body);
  console.log(body.first_name);
  res.send(body);
});
});
});


app.get(URL_BASE + 'total_users',
function(req,res){
  let tam =userFile.length;
  res.send({"msg" : "Se conto con exito el nùmero de usuarios ", tam});
});

// LOGIN mLab- users.json
app.post(URL_BASE + 'login',
  function (req, res){
    console.log("POST /apitechu/v3/login");
    let email = req.body.email;
    let pass = req.body.password;
    let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
    let limFilter = 'l=1&';
    let clienteMlab = requestJson.createClient(URL_MLAB_BASE);
    clienteMlab.get('user?'+ queryString + limFilter + apiKey_mlab,
      function(error, respuestaMLab, body) {
        if(!error) {
          if (body.length == 1) { // Existe un usuario que cumple 'queryString'
            let login = '{"$set":{"logged":true}}';
            clienteMlab.put('user?q={"id": ' + body[0].id + '}&' + apiKey_mlab, JSON.parse(login),
            //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Login correcto',
                'user':body[0].email,
                'userid':body[0].id,
                'name':body[0].first_name,
                'apellido':body[0].last_name});
                // If bodyPut.n == 1, put de mLab correcto
              });
              console.log(body[0].id);
              console.log(body);
          }
          else {
            res.status(404).send({"msg":"Usuario no válido."});
          }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  console.log("entra al write");
  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'user.json'.");
     }
   })
 };

 // LOGOUT mLab- users.json
 app.post(URL_BASE + 'logout',
   function(req, res) {
     console.log("POST /apitechu/v3/logout");
  //   var email = req.body.email;
    var id_user =req.body.id;
  //   var queryString = 'q={"email":"' + email + '","logged":true}&';
     var queryString = 'q={"id":' + id_user + ',"logged":true}&';
     console.log(queryString);
     var  clienteMlab = requestJson.createClient(URL_MLAB_BASE);
     console.log('user?'+ queryString + apiKey_mlab);
     clienteMlab.get('user?'+ queryString + apiKey_mlab,
       function(error, respuestaMLab, body) {
         var respuesta = body[0]; // Asegurar único usuario
         if(!error) {
           if (respuesta != undefined) { // Existe un usuario que cumple 'queryString'
             let logout = '{"$unset":{"logged":true}}';
             clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apiKey_mlab, JSON.parse(logout),
             //clienteMlab.put('user/' + respuesta._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
               function(errPut, resPut, bodyPut) {
                 res.send({'msg':'Logout correcto', 'user':respuesta.email});
                 // If bodyPut.n == 1, put de mLab correcto
               });
             } else {
                 res.status(404).send({"msg":"Logout failed!"});
             }
         } else {
           res.status(500).send({"msg": "Error en petición a mLab."});
         }
     });
 });

//Peticion PUT para actualizar usuario con mlab
app.put(URL_BASE + 'users/:id',
function (req,res){
  var id = req.params.id;
  var queryStringID = 'q={"id":' + id + '}&';
  var clienteMlab = requestJson.createClient(URL_MLAB_BASE);
  clienteMlab.get('user?' + queryStringID + apiKey_mlab,
function(error, respuestaMlab, body){
  var cambio ='{"$set":' + JSON.stringify(req.body) + '}';
  console.log(req.body);
  console.log(cambio);
  clienteMlab.put(URL_MLAB_BASE + 'user?' + queryStringID + apiKey_mlab, JSON.parse(cambio),
function(error, respuestaMlab, body){
  console.log("body" + body);
  res.send(req.body);
});
});

});



//Peticion PUT para actualizar usuario con mlab
app.put(URL_BASE + 'usersmLab/:id',
function (req,res){
  var id = req.params.id;
  let userBody = req.body;
  var queryStringID = 'q={"id":' + id + '}&';
  var httpclient = requestJson.createClient(URL_MLAB_BASE);
  httpclient.get('user?' + queryStringID + apiKey_mlab,
function(error, respuestaMlab, body){
  let response = body[0];
  console.log(body);

  //Actualizo campos del usuario
  let updatedUser = {
    "id" : req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": req.body.password
  };

  //Put a mLab
  httpclient.put('user/' + response._id.$oid + '?' + apiKey_mlab, updatedUser,
function(error, respuestaMlab, body){
  var response = {};
  if (error){
    response = {"msg":"Error actualizando usuario"};
    res.status(500);
  } else {
    if (body.length >0) {
      response = body;
    } else {
      response = {"msg":"Usuario actualizado correctamente"};
      res.status(404);
    }
  }
  res.send(response);
});
});

});

//Delete user with id
app.delete(URL_BASE + 'users/:id',
function(req,res){
  console.log("entra al DELETE");
  console.log("request.params.id: "+ req.params.id);
  var id= req.params.id;
  var queryStringID = 'q={"id":' + id + '}&';
  console.log(URL_MLAB_BASE + 'user?' + queryStringID + apiKey_mlab);
  var httpClient = requestJson.createClient(URL_MLAB_BASE);
  httpClient.get('user?' + queryStringID + apiKey_mlab,
    function(error,respuestaMLab, body){
      var respuesta = body[0];
      console.log(body);
      console.log("body delete:"+ JSON.stringify(respuesta));
      httpClient.delete(URL_MLAB_BASE + 'user/' + respuesta._id.$oid + '?' + apiKey_mlab,
          function(error, respuestaMLab,body){
            res.send(body);
        });
  });

});

//Peticion POST a usuario mlab - Alta new user
app.post(URL_BASE + 'afiliar',
function(req,res){
  console.log('POST/apitechu/v3/afiliar');
  var clienteMlab = requestJson.createClient(URL_MLAB_BASE);
  console.log(req.body);
  clienteMlab.get('user?'+ apiKey_mlab,
    function(error, respuestaMlab, body){
      newID = body.length + 1;
      console.log("newID: "+ newID);
      var newUser = {
       "id" : newID,
       "first_name": req.body.first_name,
       "last_name": req.body.last_name,
       "email": req.body.email,
       "password": req.body.password
      };
  clienteMlab.post("user?"+apiKey_mlab, newUser,
    function(error,respuestaMlab, body) {
      console.log("Respuesta");
      console.log(body);
      console.log(body.first_name);
      if(!error){
  //   res.send(body);
        res.send({'msg':'Alta correcta',
          'user':body.email,
          'userid':body.id,
          'name':body.first_name,
          'apellido':body.last_name});
        }
      else {
        res.status(400).send({"msg":"Error en el alta"});
      }
   });
  });
});

// Actualizar contraseña - Funcionalidad adicional
app.post(URL_BASE + 'update',
  function (req, res){
    console.log("POST /apitechu/v3/update");
    let email = req.body.email;
    let pass = req.body.password;
    let fname = req.body.first_name;
    let lname = req.body.last_name;
  //let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
    let queryString = 'q={"email":"' + email + '","first_name":"' + fname + '","last_name":"' + lname + '"}&';
    let limFilter = 'l=1&';
    let clienteMlab = requestJson.createClient(URL_MLAB_BASE);
    clienteMlab.get('user?'+ queryString + limFilter + apiKey_mlab,
      function(error, respuestaMLab, body) {
        if(!error) {
          if (body.length == 1) { // Existe un usuario que cumple 'queryString'
            var newPass = {
              "password": pass
            };
            var cambio = '{"$set":' + JSON.stringify(newPass) + '}';
            console.log("cambio", cambio);
            clienteMlab.put('user?q={"id": ' + body[0].id + '}&' + apiKey_mlab, JSON.parse(cambio),
            //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Actualizacion contraseña correcto',
                'user':body[0].email,
                'userid':body[0].id,
                'name':body[0].first_name,
                'apellido':body[0].last_name});
                // If bodyPut.n == 1, put de mLab correcto
              });
              console.log(body[0].id);
              console.log(body);
          }
          else {
            res.status(404).send({"msg":"Datos de Usuario no válido."});
          }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});
