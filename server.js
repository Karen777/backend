require ('dotenv').config();
const express = require ('express');//import
const body_parser = require('body-parser');// neceario para el POST
const app = express();
//const port = 3000;
const port = process.env.PORT || 3000;
const URL_BASE = '/apitechu/v1/';
const userFile = require('./user.json');

 // asincrono, se agrego function solo para el POST
app.listen(port,function(){
  console.log('Node JS escuchando en el puerto' + port);
});
//  function (){

//});
//necesario para el POST
app.use(body_parser.json());

//Collection
app.get(URL_BASE + 'users',
function (request,response){
  console.log('hola Perú');
  console.log('hola Lima');
  response.send(userFile);//la respuesta
});

//instancia
//peticion get a un unico usuario
app.get(URL_BASE + 'users/:id',
function (request,response){
  console.log(request.params.id);
  let pos = request.params.id -1;
  let rpta = (userFile[pos]==undefined)?{"msg":"usuario no existe"}:userFile[pos];
  let status_code = (userFile[pos]==undefined)?404:200;
  response.status(status_code).send(rpta);
});

//get con QUERY
app.get(URL_BASE + 'usersq',
function(req,res){
  console.log(req.query.id);
  console.log(req.query.email);
  res.send({"msg":"GET con query"});
});

//Peticion POST a usuario
app.post(URL_BASE + 'users',
function(req,res){
  console.log('POST a users');
  let tam = userFile.length;
  let new_user = {
    'id_user': tam + 1,
    'first_name': req.body.first_name,
    'last_name': req.body.last_name,
    'email': req.body.email,
    'pasword': req.body.pasword
  }
  console.log(new_user);
  userFile.push(new_user);
  res.send({"msg":"Usuario creado correctamente"});
});

app.get(URL_BASE + 'total_users',
function(req,res){
  let tam =userFile.length;
  res.send({"msg" : "Se conto con exito el nùmero de usuarios ", tam});
});

// LOGIN - users.json
app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("POST/apicol/v2/login");
    console.log(request.body.email);
    console.log(request.body.pasword);
    var user = request.body.email;
    var pass = request.body.pasword;
    for(us of userFile) {
      if(us.email == user) {
        if(us.pasword == pass) {
          us.logged = true;
          writeUserDataToFile(userFile);
          console.log("Login correcto!");
          response.send({"msg" : "Login correcto." , "idUsuario" : us.id_user , "logged" : "true"});
        } else {
          us.logged = false;
          writeUserDataToFile(userFile);
          console.log("Login incorrecto.");
          response.send({"msg" : "Login incorrecto."});
        }
      }
    }
});

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  console.log("entra al write");
  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'user.json'.");
     }
   })
 };

 // LOGOUT - users.json
app.post(URL_BASE + 'logout/:id',
  function(request, response) {
    console.log("POST/apicol/v2/logout");
    //var userId = request.body.id_user;
    var userId =request.params.id;
    for(us of userFile) {
      if(us.id_user == userId) {
        if(us.logged) {
          delete us.logged; // borramos propiedad 'logged'
          writeUserDataToFile(userFile);
          console.log("Logout correcto!");
          response.send({"msg" : "Logout correcto.", "idUsuario" : us.id_user});
        } else {
          console.log("Logout incorrecto.");
          response.send({"msg" : "Logout incorrecto."});
        }
      } // us.logged = true
    }
});

//Peticion PUT para actualizar usuario
app.put(URL_BASE + 'users/:id',
function (req,res){
  console.log('PUT a users');
  let new_user ={
    'id_user': req.params.id,
    'first_name': req.body.first_name,
    'last_name': req.body.last_name,
    'email': req.body.email,
    'pasword': req.body.pasword
  }
  console.log(new_user);
  userFile[req.params.id -1] = new_user;
  res.send({"msg":"usuario actualizado correctamente"});
});

app.delete(URL_BASE + 'users/:id',
function(req,res){
  console.log('DELETE user');
  userFile.splice(req.params.id-1,1);
  res.send({"msg":"usuario eliminado correctamente"});
});
